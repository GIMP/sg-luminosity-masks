---
title: sg-luminosity-masks
author: Saul Goode
license: GPLv2+
date: 2013-11-14
---

Create Luminosity Masks for an image
Originally authored by Patrick David <patdavid@gmail.com>
re-coded by Saul Goode to honor selection and layer offsets
Will isolate light, mid, and dark tones in an image as channel masks
Adapted from tutorial by Tony Kuyper (originally for PS)
http://goodlight.us/writing/luminositymasks/luminositymasks-1.html
